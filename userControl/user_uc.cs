﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace dc_power_monitor.userControl
{
    public partial class user_uc : UserControl
    {

        userHelper helper = new userHelper("");
        List<user> listUser = new List<user>();

        public user_uc()
        {
            InitializeComponent();
            if (File.Exists("user.pt"))
            {
                listUser = helper.DeSerializedUser("user.pt");
                showUserList(listUser);
            }
            
        }

        public void showUserList(List<user> list)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = list;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private user GetUser(List<user> list)
        {
            if(tbUcPasswd.Text == string.Empty | tbUcPasswd.Text == string.Empty)
            {
                return null;
            }
            user u = new user();
            u.index = list[list.Count - 1].index + 1;
            u.username = tbUcUserName.Text;
            u.passwd = tbUcPasswd.Text;
            switch (cmbLevel.SelectedIndex)
            {
                case 0:
                    u.level = authority.管理员;
                    break;
                case 1:
                    u.level = authority.工程师;
                    break;
                
                default:
                    break;
            }
            return u;
        }

        private void SetUser()
        {
            DataGridViewSelectedRowCollection rowColl = dataGridView1.SelectedRows;
            if (rowColl.Count == 0) return;
            DataGridViewRow row = rowColl[0];

            tbUcUserName.Text = row.Cells[1].Value.ToString();
            tbUcPasswd.Text = row.Cells[2].Value.ToString();

            switch (row.Cells[3].Value.ToString())
            {
                case "管理员":
                    cmbLevel.SelectedIndex = 0;
                    break;
                case "工程师":
                    cmbLevel.SelectedIndex = 1;
                    break;
                case "操作员":
                    cmbLevel.SelectedIndex = 2;
                    break;
                default:
                    break; 
            }

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            user u = GetUser(listUser);

            if(u == null)
            {
                MessageBox.Show("输入内容错误");
            }

            var res = helper.addUser("user.pt", listUser, u);
            if (res)
            {
                MessageBox.Show("添加成功");
                showUserList(listUser);
            }
            else
            {
                MessageBox.Show("添加失败");
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if(tbUcUserName.Text == string.Empty)
            {
                MessageBox.Show("删除用户名未输入");
            }
            var res = helper.delUser("user.pt", listUser, tbUcUserName.Text);
            if (res)
            {
                MessageBox.Show("删除成功");
                showUserList(listUser);
            }
            else
            {
                MessageBox.Show("删除失败");
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            SetUser();
        }
    }
}
