﻿namespace dc_power_monitor.userControl
{
    partial class home_uc
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(home_uc));
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labPow = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labCur = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labVol = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timerTest = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.txtVolSetting = new System.Windows.Forms.TextBox();
            this.txtCurSetting = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetting1 = new System.Windows.Forms.Button();
            this.btnSetting2 = new System.Windows.Forms.Button();
            this.btnSetting3 = new System.Windows.Forms.Button();
            this.btnSetting4 = new System.Windows.Forms.Button();
            this.btnSetting5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.labDevSta = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.labCtrMode = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.labVolMod = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.trcVolSet = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trcVolSet)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Image = global::dc_power_monitor.Properties.Resources.功率__1_;
            this.pictureBox3.Location = new System.Drawing.Point(241, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.tableLayoutPanel5.SetRowSpan(this.pictureBox3, 2);
            this.pictureBox3.Size = new System.Drawing.Size(153, 170);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // labPow
            // 
            this.labPow.AutoSize = true;
            this.labPow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labPow.Font = new System.Drawing.Font("Century Gothic", 54F);
            this.labPow.ForeColor = System.Drawing.Color.White;
            this.labPow.Location = new System.Drawing.Point(3, 79);
            this.labPow.Name = "labPow";
            this.labPow.Size = new System.Drawing.Size(232, 97);
            this.labPow.TabIndex = 2;
            this.labPow.Text = "38.0";
            this.labPow.Click += new System.EventHandler(this.labPow_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 28F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(232, 79);
            this.label7.TabIndex = 2;
            this.label7.Text = "Power:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::dc_power_monitor.Properties.Resources.电流__1_;
            this.pictureBox1.Location = new System.Drawing.Point(239, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel4.SetRowSpan(this.pictureBox1, 2);
            this.pictureBox1.Size = new System.Drawing.Size(152, 170);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // labCur
            // 
            this.labCur.AutoSize = true;
            this.labCur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labCur.Font = new System.Drawing.Font("Century Gothic", 54F);
            this.labCur.ForeColor = System.Drawing.Color.White;
            this.labCur.Location = new System.Drawing.Point(3, 79);
            this.labCur.Name = "labCur";
            this.labCur.Size = new System.Drawing.Size(230, 97);
            this.labCur.TabIndex = 2;
            this.labCur.Text = "40.0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 28F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 79);
            this.label3.TabIndex = 2;
            this.label3.Text = "Current:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::dc_power_monitor.Properties.Resources.电压__1_;
            this.pictureBox2.Location = new System.Drawing.Point(239, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.tableLayoutPanel3.SetRowSpan(this.pictureBox2, 2);
            this.pictureBox2.Size = new System.Drawing.Size(152, 170);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // labVol
            // 
            this.labVol.AutoSize = true;
            this.labVol.Font = new System.Drawing.Font("Century Gothic", 54F);
            this.labVol.ForeColor = System.Drawing.Color.White;
            this.labVol.Location = new System.Drawing.Point(3, 79);
            this.labVol.Name = "labVol";
            this.labVol.Size = new System.Drawing.Size(217, 87);
            this.labVol.TabIndex = 2;
            this.labVol.Text = "950.0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 28F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(230, 79);
            this.label5.TabIndex = 2;
            this.label5.Text = "Voltage:";
            // 
            // timerTest
            // 
            this.timerTest.Interval = 1000;
            this.timerTest.Tick += new System.EventHandler(this.timerTest_Tick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("幼圆", 25F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(671, 330);
            this.button1.Name = "button1";
            this.tableLayoutPanel1.SetRowSpan(this.button1, 3);
            this.button1.Size = new System.Drawing.Size(120, 261);
            this.button1.TabIndex = 16;
            this.button1.Text = "发送";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtVolSetting
            // 
            this.txtVolSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVolSetting.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVolSetting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.txtVolSetting.Location = new System.Drawing.Point(208, 16);
            this.txtVolSetting.MaxLength = 3;
            this.txtVolSetting.Name = "txtVolSetting";
            this.txtVolSetting.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVolSetting.Size = new System.Drawing.Size(322, 66);
            this.txtVolSetting.TabIndex = 14;
            this.txtVolSetting.Text = "400";
            this.txtVolSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVolSetting.TextChanged += new System.EventHandler(this.txtVolSetting_TextChanged);
            this.txtVolSetting.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVolSetting_KeyPress);
            // 
            // txtCurSetting
            // 
            this.txtCurSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurSetting.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurSetting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.txtCurSetting.Location = new System.Drawing.Point(208, 4);
            this.txtCurSetting.MaxLength = 3;
            this.txtCurSetting.Name = "txtCurSetting";
            this.txtCurSetting.Size = new System.Drawing.Size(322, 66);
            this.txtCurSetting.TabIndex = 15;
            this.txtCurSetting.Text = "30";
            this.txtCurSetting.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCurSetting.TextChanged += new System.EventHandler(this.txtCurSetting_TextChanged);
            this.txtCurSetting.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurSetting_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("幼圆", 30F);
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 40);
            this.label2.TabIndex = 12;
            this.label2.Text = "电流设置:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("幼圆", 30F);
            this.label1.Location = new System.Drawing.Point(5, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 40);
            this.label1.TabIndex = 13;
            this.label1.Text = "电压设置:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnSetting1
            // 
            this.btnSetting1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnSetting1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting1.FlatAppearance.BorderSize = 0;
            this.btnSetting1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting1.Font = new System.Drawing.Font("幼圆", 19F);
            this.btnSetting1.ForeColor = System.Drawing.Color.White;
            this.btnSetting1.Location = new System.Drawing.Point(3, 3);
            this.btnSetting1.Name = "btnSetting1";
            this.btnSetting1.Size = new System.Drawing.Size(151, 73);
            this.btnSetting1.TabIndex = 21;
            this.btnSetting1.Text = "预设1";
            this.btnSetting1.UseVisualStyleBackColor = false;
            this.btnSetting1.Click += new System.EventHandler(this.btnSetting1_Click);
            // 
            // btnSetting2
            // 
            this.btnSetting2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnSetting2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting2.FlatAppearance.BorderSize = 0;
            this.btnSetting2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting2.Font = new System.Drawing.Font("幼圆", 19F);
            this.btnSetting2.ForeColor = System.Drawing.Color.White;
            this.btnSetting2.Location = new System.Drawing.Point(160, 3);
            this.btnSetting2.Name = "btnSetting2";
            this.btnSetting2.Size = new System.Drawing.Size(151, 73);
            this.btnSetting2.TabIndex = 21;
            this.btnSetting2.Text = "预设2";
            this.btnSetting2.UseVisualStyleBackColor = false;
            this.btnSetting2.Click += new System.EventHandler(this.btnSetting2_Click);
            // 
            // btnSetting3
            // 
            this.btnSetting3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnSetting3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting3.FlatAppearance.BorderSize = 0;
            this.btnSetting3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting3.Font = new System.Drawing.Font("幼圆", 19F);
            this.btnSetting3.ForeColor = System.Drawing.Color.White;
            this.btnSetting3.Location = new System.Drawing.Point(317, 3);
            this.btnSetting3.Name = "btnSetting3";
            this.btnSetting3.Size = new System.Drawing.Size(151, 73);
            this.btnSetting3.TabIndex = 21;
            this.btnSetting3.Text = "预设3";
            this.btnSetting3.UseVisualStyleBackColor = false;
            this.btnSetting3.Click += new System.EventHandler(this.btnSetting3_Click);
            // 
            // btnSetting4
            // 
            this.btnSetting4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnSetting4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting4.FlatAppearance.BorderSize = 0;
            this.btnSetting4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting4.Font = new System.Drawing.Font("幼圆", 19F);
            this.btnSetting4.ForeColor = System.Drawing.Color.White;
            this.btnSetting4.Location = new System.Drawing.Point(474, 3);
            this.btnSetting4.Name = "btnSetting4";
            this.btnSetting4.Size = new System.Drawing.Size(151, 73);
            this.btnSetting4.TabIndex = 21;
            this.btnSetting4.Text = "预设4";
            this.btnSetting4.UseVisualStyleBackColor = false;
            this.btnSetting4.Click += new System.EventHandler(this.btnSetting4_Click);
            // 
            // btnSetting5
            // 
            this.btnSetting5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnSetting5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetting5.FlatAppearance.BorderSize = 0;
            this.btnSetting5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting5.Font = new System.Drawing.Font("幼圆", 19F);
            this.btnSetting5.ForeColor = System.Drawing.Color.White;
            this.btnSetting5.Location = new System.Drawing.Point(631, 3);
            this.btnSetting5.Name = "btnSetting5";
            this.btnSetting5.Size = new System.Drawing.Size(154, 73);
            this.btnSetting5.TabIndex = 21;
            this.btnSetting5.Text = "预设5";
            this.btnSetting5.UseVisualStyleBackColor = false;
            this.btnSetting5.Click += new System.EventHandler(this.btnSetting5_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("幼圆", 22F);
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 54);
            this.label4.TabIndex = 19;
            this.label4.Text = "设备状态：";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labDevSta
            // 
            this.labDevSta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labDevSta.AutoSize = true;
            this.labDevSta.Font = new System.Drawing.Font("幼圆", 22F);
            this.labDevSta.ForeColor = System.Drawing.Color.Green;
            this.labDevSta.Location = new System.Drawing.Point(200, 0);
            this.labDevSta.Name = "labDevSta";
            this.labDevSta.Size = new System.Drawing.Size(73, 54);
            this.labDevSta.TabIndex = 20;
            this.labDevSta.Text = "空闲";
            this.labDevSta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.DimGray;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStop.FlatAppearance.BorderSize = 0;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("幼圆", 56F);
            this.btnStop.ForeColor = System.Drawing.Color.White;
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStop.Location = new System.Drawing.Point(803, 421);
            this.btnStop.Name = "btnStop";
            this.tableLayoutPanel1.SetRowSpan(this.btnStop, 2);
            this.btnStop.Size = new System.Drawing.Size(397, 170);
            this.btnStop.TabIndex = 31;
            this.btnStop.Text = " 关  机";
            this.btnStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("幼圆", 56F);
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.Location = new System.Drawing.Point(803, 245);
            this.btnStart.Name = "btnStart";
            this.tableLayoutPanel1.SetRowSpan(this.btnStart, 2);
            this.btnStart.Size = new System.Drawing.Size(397, 170);
            this.btnStart.TabIndex = 30;
            this.btnStart.Text = " 开  机";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 3);
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 245);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(788, 79);
            this.panel4.TabIndex = 32;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.btnSetting1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSetting5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSetting2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSetting4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSetting3, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(788, 79);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.trcVolSet, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnStart, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnStop, 4, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1203, 608);
            this.tableLayoutPanel1.TabIndex = 35;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Crimson;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.Controls.Add(this.pictureBox3, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.labPow, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(803, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(397, 176);
            this.tableLayoutPanel5.TabIndex = 37;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.ForestGreen;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.Controls.Add(this.pictureBox2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labVol, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(394, 176);
            this.tableLayoutPanel3.TabIndex = 36;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Peru;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.labCur, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(403, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(394, 176);
            this.tableLayoutPanel4.TabIndex = 36;
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtVolSetting);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 330);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(662, 85);
            this.panel2.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.label10.Location = new System.Drawing.Point(536, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 23);
            this.label10.TabIndex = 16;
            this.label10.Text = "(200~950 V)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // panel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel5, 2);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.txtCurSetting);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 512);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(662, 79);
            this.panel5.TabIndex = 38;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.label9.Location = new System.Drawing.Point(536, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 23);
            this.label9.TabIndex = 17;
            this.label9.Text = "(0 ~ 160 A)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.labDevSta, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 185);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(394, 54);
            this.tableLayoutPanel6.TabIndex = 39;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 597);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1197, 8);
            this.panel1.TabIndex = 40;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel7, 2);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.labCtrMode, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(403, 185);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(388, 54);
            this.tableLayoutPanel7.TabIndex = 41;
            // 
            // labCtrMode
            // 
            this.labCtrMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labCtrMode.AutoSize = true;
            this.labCtrMode.Font = new System.Drawing.Font("幼圆", 22F);
            this.labCtrMode.ForeColor = System.Drawing.Color.Green;
            this.labCtrMode.Location = new System.Drawing.Point(197, 0);
            this.labCtrMode.Name = "labCtrMode";
            this.labCtrMode.Size = new System.Drawing.Size(73, 54);
            this.labCtrMode.TabIndex = 21;
            this.labCtrMode.Text = "远程";
            this.labCtrMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("幼圆", 22F);
            this.label6.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(188, 54);
            this.label6.TabIndex = 21;
            this.label6.Text = "控制模式：";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.labVolMod, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(803, 185);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(397, 54);
            this.tableLayoutPanel8.TabIndex = 42;
            // 
            // labVolMod
            // 
            this.labVolMod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labVolMod.AutoSize = true;
            this.labVolMod.Font = new System.Drawing.Font("幼圆", 22F);
            this.labVolMod.ForeColor = System.Drawing.Color.Green;
            this.labVolMod.Location = new System.Drawing.Point(201, 0);
            this.labVolMod.Name = "labVolMod";
            this.labVolMod.Size = new System.Drawing.Size(73, 54);
            this.labVolMod.TabIndex = 23;
            this.labVolMod.Text = "高压";
            this.labVolMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("幼圆", 22F);
            this.label11.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(192, 54);
            this.label11.TabIndex = 22;
            this.label11.Text = "电压模式：";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // trcVolSet
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.trcVolSet, 2);
            this.trcVolSet.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.trcVolSet.LargeChange = 1;
            this.trcVolSet.Location = new System.Drawing.Point(3, 461);
            this.trcVolSet.Maximum = 950;
            this.trcVolSet.Minimum = 200;
            this.trcVolSet.Name = "trcVolSet";
            this.trcVolSet.Size = new System.Drawing.Size(662, 45);
            this.trcVolSet.TabIndex = 17;
            this.trcVolSet.Value = 400;
            this.trcVolSet.Scroll += new System.EventHandler(this.trcVolSet_Scroll);
            // 
            // home_uc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "home_uc";
            this.Size = new System.Drawing.Size(1203, 608);
            this.Load += new System.EventHandler(this.home_uc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trcVolSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labPow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labCur;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label labVol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timerTest;
        private System.Windows.Forms.PictureBox pictureBox1;
        
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtVolSetting;
        private System.Windows.Forms.TextBox txtCurSetting;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSetting1;
        private System.Windows.Forms.Button btnSetting2;
        private System.Windows.Forms.Button btnSetting3;
        private System.Windows.Forms.Button btnSetting4;
        private System.Windows.Forms.Button btnSetting5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labDevSta;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label labCtrMode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labVolMod;
        private System.Windows.Forms.TrackBar trcVolSet;
    }
}
