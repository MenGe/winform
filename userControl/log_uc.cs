﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace dc_power_monitor.userControl
{
    public partial class log_uc : UserControl
    {
        recordHelper rec;

        private authority level = authority.工程师;

        public log_uc(authority le)
        {
            InitializeComponent();

            level = le;

            rec = new recordHelper("log_opreat_fault.db");

            dataGridView1.DataSource = rec.getOpreatRecord();

            btnOpreatRec.BackColor = System.Drawing.Color.Gainsboro;
            btnErrorRec.BackColor = System.Drawing.Color.Transparent;
        }

        private void btnErrorRec_Click(object sender, EventArgs e)
        {
            
            dataGridView1.DataSource = rec.getFaultRecord();

           

            btnErrorRec.BackColor = System.Drawing.Color.Gainsboro;
            btnOpreatRec.BackColor = System.Drawing.Color.Transparent;
        }

        public void update_log()
        {
            dataGridView1.DataSource = rec.getOpreatRecord();


            btnOpreatRec.BackColor = System.Drawing.Color.Gainsboro;
            btnErrorRec.BackColor = System.Drawing.Color.Transparent;
        }

        private void btnOpreatRec_Click(object sender, EventArgs e)
        {

            dataGridView1.DataSource = rec.getOpreatRecord();


            btnOpreatRec.BackColor = System.Drawing.Color.Gainsboro;
            btnErrorRec.BackColor = System.Drawing.Color.Transparent;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnDelOpreatRecord_Click(object sender, EventArgs e)
        {
            if(level == authority.管理员)
            {
                if (MessageBox.Show("确认删除？", "此删除不可恢复", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    rec.delTable("opreat");

                    dataGridView1.DataSource = rec.getOpreatRecord();
                }
            }
            else
            {
                MessageBox.Show("权限不足，需要管理员权限");
            }
            
        }

        private void btnDelFaultRecord_Click(object sender, EventArgs e)
        {
            if (level == authority.管理员)
            {
                if (MessageBox.Show("确认删除？", "此删除不可恢复", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    rec.delTable("fault");

                    dataGridView1.DataSource = rec.getFaultRecord();
                }
            }
            else
            {
                MessageBox.Show("权限不足，需要管理员权限");
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Enter(object sender, EventArgs e)
        {
            dataGridView1.DataSource = rec.getOpreatRecord();

            btnOpreatRec.BackColor = System.Drawing.Color.Gainsboro;
            btnErrorRec.BackColor = System.Drawing.Color.Transparent;
        }

        private void tableLayoutPanel1_Validated(object sender, EventArgs e)
        {
            dataGridView1.DataSource = rec.getOpreatRecord();

            btnOpreatRec.BackColor = System.Drawing.Color.Gainsboro;
            btnErrorRec.BackColor = System.Drawing.Color.Transparent;
        }
    }
}
