﻿namespace dc_power_monitor.userControl
{
    partial class log_uc
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDelFaultRecord = new System.Windows.Forms.Button();
            this.btnDelOpreatRecord = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnOpreatRec = new System.Windows.Forms.Button();
            this.btnErrorRec = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.Controls.Add(this.btnDelFaultRecord, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDelOpreatRecord, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnOpreatRec, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnErrorRec, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1203, 608);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            this.tableLayoutPanel1.Enter += new System.EventHandler(this.tableLayoutPanel1_Enter);
            this.tableLayoutPanel1.Validated += new System.EventHandler(this.tableLayoutPanel1_Validated);
            // 
            // btnDelFaultRecord
            // 
            this.btnDelFaultRecord.BackColor = System.Drawing.Color.Transparent;
            this.btnDelFaultRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelFaultRecord.FlatAppearance.BorderSize = 0;
            this.btnDelFaultRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelFaultRecord.Location = new System.Drawing.Point(3, 549);
            this.btnDelFaultRecord.Name = "btnDelFaultRecord";
            this.btnDelFaultRecord.Size = new System.Drawing.Size(114, 56);
            this.btnDelFaultRecord.TabIndex = 3;
            this.btnDelFaultRecord.Text = "清空故障记录";
            this.btnDelFaultRecord.UseVisualStyleBackColor = false;
            this.btnDelFaultRecord.Click += new System.EventHandler(this.btnDelFaultRecord_Click);
            // 
            // btnDelOpreatRecord
            // 
            this.btnDelOpreatRecord.BackColor = System.Drawing.Color.Transparent;
            this.btnDelOpreatRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelOpreatRecord.FlatAppearance.BorderSize = 0;
            this.btnDelOpreatRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelOpreatRecord.Location = new System.Drawing.Point(3, 246);
            this.btnDelOpreatRecord.Name = "btnDelOpreatRecord";
            this.btnDelOpreatRecord.Size = new System.Drawing.Size(114, 54);
            this.btnDelOpreatRecord.TabIndex = 2;
            this.btnDelOpreatRecord.Text = "清空操作记录";
            this.btnDelOpreatRecord.UseVisualStyleBackColor = false;
            this.btnDelOpreatRecord.Click += new System.EventHandler(this.btnDelOpreatRecord_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(123, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView1, 4);
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1077, 602);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnOpreatRec
            // 
            this.btnOpreatRec.BackColor = System.Drawing.Color.Transparent;
            this.btnOpreatRec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpreatRec.FlatAppearance.BorderSize = 0;
            this.btnOpreatRec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpreatRec.Location = new System.Drawing.Point(3, 3);
            this.btnOpreatRec.Name = "btnOpreatRec";
            this.btnOpreatRec.Size = new System.Drawing.Size(114, 237);
            this.btnOpreatRec.TabIndex = 1;
            this.btnOpreatRec.Text = "操作记录";
            this.btnOpreatRec.UseVisualStyleBackColor = false;
            this.btnOpreatRec.Click += new System.EventHandler(this.btnOpreatRec_Click);
            // 
            // btnErrorRec
            // 
            this.btnErrorRec.BackColor = System.Drawing.Color.Transparent;
            this.btnErrorRec.FlatAppearance.BorderSize = 0;
            this.btnErrorRec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnErrorRec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnErrorRec.Location = new System.Drawing.Point(3, 306);
            this.btnErrorRec.Name = "btnErrorRec";
            this.btnErrorRec.Size = new System.Drawing.Size(114, 237);
            this.btnErrorRec.TabIndex = 1;
            this.btnErrorRec.Text = "故障记录";
            this.btnErrorRec.UseVisualStyleBackColor = false;
            this.btnErrorRec.Click += new System.EventHandler(this.btnErrorRec_Click);
            // 
            // log_uc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "log_uc";
            this.Size = new System.Drawing.Size(1203, 608);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnErrorRec;
        private System.Windows.Forms.Button btnOpreatRec;
        private System.Windows.Forms.Button btnDelFaultRecord;
        private System.Windows.Forms.Button btnDelOpreatRecord;
    }
}
