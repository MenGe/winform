﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Configuration;
using Newtonsoft.Json;

namespace dc_power_monitor.userControl
{
    public partial class setting_uc : UserControl
    {
        private recordHelper rec;
        private inputParam param;
        private modParam param_mod;

        public delegate void sendSerialData(ushort add, bool value);
        public sendSerialData sendSerial_set;

        public setting_uc()
        {
            InitializeComponent();

            rec = new recordHelper("log_opreat_fault.db");

            cmbComList.Text = ConfigurationManager.AppSettings["COM"];
        }

        private void updateComList()
        {
            string[] ArryPort = SerialPort.GetPortNames();  //定义字符串数组，数组名为 ArryPort
                                                            //SerialP ort.GetPortNames()函数功能为获取计算机所有可用串口，以字符串数组形式输出
            cmbComList.Items.Clear();       //清除当前组合框下拉菜单内容                  

            for (int i = 0; i < ArryPort.Length; i++)
            {
                cmbComList.Items.Add(ArryPort[i]);   //将所有的可用串口号添加到  端口 对应的组合框中
            }
        }

        private delegate void SetTextCallBack(string text, Label lab);
        private void SetText(SetTextCallBack func, string text, Label lab)
        {
            if (lab.InvokeRequired)
            {

                lab.Invoke(func, new object[] { text, lab });
            }
            else
            {
                func(text, lab);
            }
        }

        private void LoadData(string txt, Label lab)
        {
            lab.Text = txt;
        }

        private string intString2FloatString(string str, string unit)
        {
            int ret = Convert.ToInt32(str);

            return (ret / 10).ToString() + "." + (ret % 10).ToString() + unit;
        }

        public void RecieveData(inputParam pp)
        {
            param = pp;

            SetText(LoadData, intString2FloatString(param.inCur, " A"), labInCur);
            SetText(LoadData, intString2FloatString(param.inHz, " Hz"), labInHz);
            SetText(LoadData, intString2FloatString(param.inUV, " V"), labInUV);
            SetText(LoadData, intString2FloatString(param.inVW, " V"), labInVW);
            SetText(LoadData, intString2FloatString(param.inWU, " V"), labInWU);
        }

        public void RecieveData_mod(modParam ppp)
        {
            param_mod = ppp;



            if (param_mod.mod_online[0] == true)
            {
                SetText(LoadData, "在线", labOnlineFlag1);
            }
            else
            {
                SetText(LoadData, "离线", labOnlineFlag1);
            }

            if (param_mod.mod_online[1] == true)
            {
                SetText(LoadData, "在线", labOnlineFlag2);
            }
            else
            {
                SetText(LoadData, "离线", labOnlineFlag2);
            }

            if (param_mod.mod_online[2] == true)
            {
                SetText(LoadData, "在线", labOnlineFlag3);
            }
            else
            {
                SetText(LoadData, "离线", labOnlineFlag3);
            }

            if (param_mod.mod_online[3] == true)
            {
                SetText(LoadData, "在线", labOnlineFlag4);
            }
            else
            {
                SetText(LoadData, "离线", labOnlineFlag4);
            }

            SetText(LoadData, intString2FloatString(param_mod.mod_out_i[0].ToString(), " A"), labMod1Cur);
            SetText(LoadData, intString2FloatString(param_mod.mod_out_i[1].ToString(), " A"), labMod2Cur);
            SetText(LoadData, intString2FloatString(param_mod.mod_out_i[2].ToString(), " A"), labMod3Cur);
            SetText(LoadData, intString2FloatString(param_mod.mod_out_i[3].ToString(), " A"), labMod4Cur);
        }



            private void setting_uc_Load(object sender, EventArgs e)
        {
            updateComList();

            string vol_cur = ConfigurationManager.AppSettings["set1"];
            string[] volCur = vol_cur.Split(' ');

            txtSetVol1.Text = volCur[0];
            txtSetCur1.Text = volCur[1];

            vol_cur = ConfigurationManager.AppSettings["set2"];
            volCur = vol_cur.Split(' ');

            txtSetVol2.Text = volCur[0];
            txtSetCur2.Text = volCur[1];


            vol_cur = ConfigurationManager.AppSettings["set3"];
            volCur = vol_cur.Split(' ');

            txtSetVol3.Text = volCur[0];
            txtSetCur3.Text = volCur[1];


            vol_cur = ConfigurationManager.AppSettings["set4"];
            volCur = vol_cur.Split(' ');

            txtSetVol4.Text = volCur[0];
            txtSetCur4.Text = volCur[1];


            vol_cur = ConfigurationManager.AppSettings["set5"];
            volCur = vol_cur.Split(' ');

            txtSetVol5.Text = volCur[0];
            txtSetCur5.Text = volCur[1];
        }

        private void cmbComList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["COM"].Value = cmbComList.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存
            
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetVol1_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set1"].Value = txtSetVol1.Text + ' ' + txtSetCur1.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetCur1_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set1"].Value = txtSetVol1.Text + ' ' + txtSetCur1.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetVol2_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set2"].Value = txtSetVol2.Text + ' ' + txtSetCur2.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetCur2_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set2"].Value = txtSetVol2.Text + ' ' + txtSetCur2.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetVol3_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set3"].Value = txtSetVol3.Text + ' ' + txtSetCur3.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetCur3_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set3"].Value = txtSetVol3.Text + ' ' + txtSetCur3.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetVol4_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set4"].Value = txtSetVol4.Text + ' ' + txtSetCur4.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetCur4_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set4"].Value = txtSetVol4.Text + ' ' + txtSetCur4.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetVol5_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set5"].Value = txtSetVol5.Text + ' ' + txtSetCur5.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtSetCur5_TextChanged(object sender, EventArgs e)
        {
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["set5"].Value = txtSetVol5.Text + ' ' + txtSetCur5.Text;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void cmbCtrMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string opreat = "切换控制模式：" + cmbCtrMode.Text;

            rec.addOpreatRecord(ConfigurationManager.AppSettings["user"], opreat);
            sendSerial_set.BeginInvoke(0x03, true, null, null);
            if (cmbCtrMode.Text == "远程")
                sendSerial_set.BeginInvoke(0x02, true, null, null);
            if(cmbCtrMode.Text == "本地")
                sendSerial_set.BeginInvoke(0x02, false, null, null);
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void labInCur_Click(object sender, EventArgs e)
        {

        }

        private void labInHz_Click(object sender, EventArgs e)
        {

        }

        private void labInVW_Click(object sender, EventArgs e)
        {

        }

        private void labInWU_Click(object sender, EventArgs e)
        {

        }

        private void labInUV_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click_1(object sender, EventArgs e)
        {

        }

        private void cmbVolType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string opreat = "切换电压模式：" + cmbVolType.Text;

            rec.addOpreatRecord(ConfigurationManager.AppSettings["user"], opreat);
            sendSerial_set.BeginInvoke(0x03, true, null, null);
            if (cmbVolType.Text == "高压模式")
                sendSerial_set.BeginInvoke(0x04, false, null, null);
            if (cmbVolType.Text == "低压模式")
                sendSerial_set.BeginInvoke(0x04, true, null, null);
        }
    }
}
