﻿namespace dc_power_monitor.userControl
{
    partial class setting_uc
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbComList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSetVol1 = new System.Windows.Forms.TextBox();
            this.txtSetCur1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSetCur2 = new System.Windows.Forms.TextBox();
            this.txtSetVol2 = new System.Windows.Forms.TextBox();
            this.txtSetCur3 = new System.Windows.Forms.TextBox();
            this.txtSetVol3 = new System.Windows.Forms.TextBox();
            this.txtSetCur4 = new System.Windows.Forms.TextBox();
            this.txtSetVol4 = new System.Windows.Forms.TextBox();
            this.txtSetCur5 = new System.Windows.Forms.TextBox();
            this.txtSetVol5 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labInCur = new System.Windows.Forms.Label();
            this.labInHz = new System.Windows.Forms.Label();
            this.labInVW = new System.Windows.Forms.Label();
            this.labInWU = new System.Windows.Forms.Label();
            this.labInUV = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbCtrMode = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labOnlineFlag1 = new System.Windows.Forms.Label();
            this.labMod1Cur = new System.Windows.Forms.Label();
            this.labFaultMod1 = new System.Windows.Forms.Label();
            this.labFaultMod2 = new System.Windows.Forms.Label();
            this.labMod2Cur = new System.Windows.Forms.Label();
            this.labOnlineFlag2 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.labFaultMod3 = new System.Windows.Forms.Label();
            this.labMod3Cur = new System.Windows.Forms.Label();
            this.labOnlineFlag3 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.labFaultMod4 = new System.Windows.Forms.Label();
            this.labMod4Cur = new System.Windows.Forms.Label();
            this.labOnlineFlag4 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbVolType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmbComList
            // 
            this.cmbComList.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.cmbComList.FormattingEnabled = true;
            this.cmbComList.Location = new System.Drawing.Point(268, 53);
            this.cmbComList.Name = "cmbComList";
            this.cmbComList.Size = new System.Drawing.Size(195, 32);
            this.cmbComList.TabIndex = 0;
            this.cmbComList.SelectedIndexChanged += new System.EventHandler(this.cmbComList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label1.Location = new System.Drawing.Point(98, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "COM:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("幼圆", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label2.Location = new System.Drawing.Point(99, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "预设1：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("幼圆", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label3.Location = new System.Drawing.Point(99, 382);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "预设2：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("幼圆", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label4.Location = new System.Drawing.Point(99, 439);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "预设3：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("幼圆", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label5.Location = new System.Drawing.Point(99, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "预设4：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("幼圆", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label6.Location = new System.Drawing.Point(99, 553);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "预设5：";
            // 
            // txtSetVol1
            // 
            this.txtSetVol1.Location = new System.Drawing.Point(208, 324);
            this.txtSetVol1.Name = "txtSetVol1";
            this.txtSetVol1.Size = new System.Drawing.Size(100, 27);
            this.txtSetVol1.TabIndex = 3;
            this.txtSetVol1.TextChanged += new System.EventHandler(this.txtSetVol1_TextChanged);
            // 
            // txtSetCur1
            // 
            this.txtSetCur1.Location = new System.Drawing.Point(363, 324);
            this.txtSetCur1.Name = "txtSetCur1";
            this.txtSetCur1.Size = new System.Drawing.Size(100, 27);
            this.txtSetCur1.TabIndex = 4;
            this.txtSetCur1.TextChanged += new System.EventHandler(this.txtSetCur1_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label7.Location = new System.Drawing.Point(227, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 25);
            this.label7.TabIndex = 1;
            this.label7.Text = "电压";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label8.Location = new System.Drawing.Point(381, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "电流";
            // 
            // txtSetCur2
            // 
            this.txtSetCur2.Location = new System.Drawing.Point(363, 381);
            this.txtSetCur2.Name = "txtSetCur2";
            this.txtSetCur2.Size = new System.Drawing.Size(100, 27);
            this.txtSetCur2.TabIndex = 6;
            this.txtSetCur2.TextChanged += new System.EventHandler(this.txtSetCur2_TextChanged);
            // 
            // txtSetVol2
            // 
            this.txtSetVol2.Location = new System.Drawing.Point(208, 381);
            this.txtSetVol2.Name = "txtSetVol2";
            this.txtSetVol2.Size = new System.Drawing.Size(100, 27);
            this.txtSetVol2.TabIndex = 5;
            this.txtSetVol2.TextChanged += new System.EventHandler(this.txtSetVol2_TextChanged);
            // 
            // txtSetCur3
            // 
            this.txtSetCur3.Location = new System.Drawing.Point(363, 438);
            this.txtSetCur3.Name = "txtSetCur3";
            this.txtSetCur3.Size = new System.Drawing.Size(100, 27);
            this.txtSetCur3.TabIndex = 8;
            this.txtSetCur3.TextChanged += new System.EventHandler(this.txtSetCur3_TextChanged);
            // 
            // txtSetVol3
            // 
            this.txtSetVol3.Location = new System.Drawing.Point(208, 438);
            this.txtSetVol3.Name = "txtSetVol3";
            this.txtSetVol3.Size = new System.Drawing.Size(100, 27);
            this.txtSetVol3.TabIndex = 7;
            this.txtSetVol3.TextChanged += new System.EventHandler(this.txtSetVol3_TextChanged);
            // 
            // txtSetCur4
            // 
            this.txtSetCur4.Location = new System.Drawing.Point(363, 495);
            this.txtSetCur4.Name = "txtSetCur4";
            this.txtSetCur4.Size = new System.Drawing.Size(100, 27);
            this.txtSetCur4.TabIndex = 10;
            this.txtSetCur4.TextChanged += new System.EventHandler(this.txtSetCur4_TextChanged);
            // 
            // txtSetVol4
            // 
            this.txtSetVol4.Location = new System.Drawing.Point(208, 495);
            this.txtSetVol4.Name = "txtSetVol4";
            this.txtSetVol4.Size = new System.Drawing.Size(100, 27);
            this.txtSetVol4.TabIndex = 9;
            this.txtSetVol4.TextChanged += new System.EventHandler(this.txtSetVol4_TextChanged);
            // 
            // txtSetCur5
            // 
            this.txtSetCur5.Location = new System.Drawing.Point(363, 552);
            this.txtSetCur5.Name = "txtSetCur5";
            this.txtSetCur5.Size = new System.Drawing.Size(100, 27);
            this.txtSetCur5.TabIndex = 12;
            this.txtSetCur5.TextChanged += new System.EventHandler(this.txtSetCur5_TextChanged);
            // 
            // txtSetVol5
            // 
            this.txtSetVol5.Location = new System.Drawing.Point(208, 552);
            this.txtSetVol5.Name = "txtSetVol5";
            this.txtSetVol5.Size = new System.Drawing.Size(100, 27);
            this.txtSetVol5.TabIndex = 11;
            this.txtSetVol5.TextChanged += new System.EventHandler(this.txtSetVol5_TextChanged);
            // 
            // label27
            // 
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Location = new System.Drawing.Point(505, 10);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(2, 588);
            this.label27.TabIndex = 53;
            this.label27.Text = "label27";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("幼圆", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label26.Location = new System.Drawing.Point(536, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(185, 24);
            this.label26.TabIndex = 52;
            this.label26.Text = "交流输入侧数据";
            this.label26.Click += new System.EventHandler(this.label26_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("幼圆", 15F);
            this.label24.Location = new System.Drawing.Point(541, 257);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(109, 20);
            this.label24.TabIndex = 50;
            this.label24.Text = "线电压WU：";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("幼圆", 15F);
            this.label23.Location = new System.Drawing.Point(541, 225);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(109, 20);
            this.label23.TabIndex = 49;
            this.label23.Text = "线电压VW：";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("幼圆", 15F);
            this.label21.Location = new System.Drawing.Point(541, 193);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(109, 20);
            this.label21.TabIndex = 48;
            this.label21.Text = "线电压UV：";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("幼圆", 15F);
            this.label20.Location = new System.Drawing.Point(542, 161);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(109, 20);
            this.label20.TabIndex = 47;
            this.label20.Text = "输入频率：";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // labInCur
            // 
            this.labInCur.AutoSize = true;
            this.labInCur.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInCur.ForeColor = System.Drawing.Color.Green;
            this.labInCur.Location = new System.Drawing.Point(699, 124);
            this.labInCur.Name = "labInCur";
            this.labInCur.Size = new System.Drawing.Size(63, 23);
            this.labInCur.TabIndex = 43;
            this.labInCur.Text = "50.0A";
            this.labInCur.Click += new System.EventHandler(this.labInCur_Click);
            // 
            // labInHz
            // 
            this.labInHz.AutoSize = true;
            this.labInHz.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInHz.ForeColor = System.Drawing.Color.Green;
            this.labInHz.Location = new System.Drawing.Point(699, 156);
            this.labInHz.Name = "labInHz";
            this.labInHz.Size = new System.Drawing.Size(71, 23);
            this.labInHz.TabIndex = 42;
            this.labInHz.Text = "50.0Hz";
            this.labInHz.Click += new System.EventHandler(this.labInHz_Click);
            // 
            // labInVW
            // 
            this.labInVW.AutoSize = true;
            this.labInVW.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInVW.ForeColor = System.Drawing.Color.Green;
            this.labInVW.Location = new System.Drawing.Point(699, 220);
            this.labInVW.Name = "labInVW";
            this.labInVW.Size = new System.Drawing.Size(74, 23);
            this.labInVW.TabIndex = 40;
            this.labInVW.Text = "382.0V";
            this.labInVW.Click += new System.EventHandler(this.labInVW_Click);
            // 
            // labInWU
            // 
            this.labInWU.AutoSize = true;
            this.labInWU.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInWU.ForeColor = System.Drawing.Color.Green;
            this.labInWU.Location = new System.Drawing.Point(699, 252);
            this.labInWU.Name = "labInWU";
            this.labInWU.Size = new System.Drawing.Size(74, 23);
            this.labInWU.TabIndex = 39;
            this.labInWU.Text = "382.0V";
            this.labInWU.Click += new System.EventHandler(this.labInWU_Click);
            // 
            // labInUV
            // 
            this.labInUV.AutoSize = true;
            this.labInUV.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInUV.ForeColor = System.Drawing.Color.Green;
            this.labInUV.Location = new System.Drawing.Point(699, 188);
            this.labInUV.Name = "labInUV";
            this.labInUV.Size = new System.Drawing.Size(74, 23);
            this.labInUV.TabIndex = 38;
            this.labInUV.Text = "382.0V";
            this.labInUV.Click += new System.EventHandler(this.labInUV_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("幼圆", 15F);
            this.label13.Location = new System.Drawing.Point(542, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 20);
            this.label13.TabIndex = 31;
            this.label13.Text = "输入电流：";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("幼圆", 16F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label11.Location = new System.Drawing.Point(98, 132);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 22);
            this.label11.TabIndex = 55;
            this.label11.Text = "控制模式:";
            // 
            // cmbCtrMode
            // 
            this.cmbCtrMode.Font = new System.Drawing.Font("幼圆", 16F);
            this.cmbCtrMode.FormattingEnabled = true;
            this.cmbCtrMode.Items.AddRange(new object[] {
            "本地",
            "远程"});
            this.cmbCtrMode.Location = new System.Drawing.Point(268, 130);
            this.cmbCtrMode.Name = "cmbCtrMode";
            this.cmbCtrMode.Size = new System.Drawing.Size(195, 29);
            this.cmbCtrMode.TabIndex = 56;
            this.cmbCtrMode.SelectedIndexChanged += new System.EventHandler(this.cmbCtrMode_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(541, 297);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(588, 2);
            this.label9.TabIndex = 57;
            this.label9.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("幼圆", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label10.Location = new System.Drawing.Point(536, 320);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 24);
            this.label10.TabIndex = 58;
            this.label10.Text = "模块数据";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("幼圆", 12F);
            this.label12.Location = new System.Drawing.Point(536, 382);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 16);
            this.label12.TabIndex = 59;
            this.label12.Text = "模块1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("幼圆", 12F);
            this.label14.Location = new System.Drawing.Point(613, 382);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 60;
            this.label14.Text = "在线状态：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("幼圆", 12F);
            this.label15.Location = new System.Drawing.Point(804, 382);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 16);
            this.label15.TabIndex = 61;
            this.label15.Text = "输出电流：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("幼圆", 12F);
            this.label16.Location = new System.Drawing.Point(1010, 382);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 16);
            this.label16.TabIndex = 62;
            this.label16.Text = "故障状态：";
            // 
            // labOnlineFlag1
            // 
            this.labOnlineFlag1.AutoSize = true;
            this.labOnlineFlag1.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labOnlineFlag1.ForeColor = System.Drawing.Color.Green;
            this.labOnlineFlag1.Location = new System.Drawing.Point(735, 378);
            this.labOnlineFlag1.Name = "labOnlineFlag1";
            this.labOnlineFlag1.Size = new System.Drawing.Size(46, 21);
            this.labOnlineFlag1.TabIndex = 63;
            this.labOnlineFlag1.Text = "在线";
            this.labOnlineFlag1.Click += new System.EventHandler(this.label17_Click);
            // 
            // labMod1Cur
            // 
            this.labMod1Cur.AutoSize = true;
            this.labMod1Cur.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labMod1Cur.ForeColor = System.Drawing.Color.Green;
            this.labMod1Cur.Location = new System.Drawing.Point(926, 378);
            this.labMod1Cur.Name = "labMod1Cur";
            this.labMod1Cur.Size = new System.Drawing.Size(60, 21);
            this.labMod1Cur.TabIndex = 64;
            this.labMod1Cur.Text = "50.0A";
            // 
            // labFaultMod1
            // 
            this.labFaultMod1.AutoSize = true;
            this.labFaultMod1.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labFaultMod1.ForeColor = System.Drawing.Color.Green;
            this.labFaultMod1.Location = new System.Drawing.Point(1132, 378);
            this.labFaultMod1.Name = "labFaultMod1";
            this.labFaultMod1.Size = new System.Drawing.Size(28, 21);
            this.labFaultMod1.TabIndex = 65;
            this.labFaultMod1.Text = "无";
            // 
            // labFaultMod2
            // 
            this.labFaultMod2.AutoSize = true;
            this.labFaultMod2.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labFaultMod2.ForeColor = System.Drawing.Color.Green;
            this.labFaultMod2.Location = new System.Drawing.Point(1132, 419);
            this.labFaultMod2.Name = "labFaultMod2";
            this.labFaultMod2.Size = new System.Drawing.Size(28, 21);
            this.labFaultMod2.TabIndex = 72;
            this.labFaultMod2.Text = "无";
            // 
            // labMod2Cur
            // 
            this.labMod2Cur.AutoSize = true;
            this.labMod2Cur.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labMod2Cur.ForeColor = System.Drawing.Color.Green;
            this.labMod2Cur.Location = new System.Drawing.Point(926, 419);
            this.labMod2Cur.Name = "labMod2Cur";
            this.labMod2Cur.Size = new System.Drawing.Size(60, 21);
            this.labMod2Cur.TabIndex = 71;
            this.labMod2Cur.Text = "50.0A";
            // 
            // labOnlineFlag2
            // 
            this.labOnlineFlag2.AutoSize = true;
            this.labOnlineFlag2.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labOnlineFlag2.ForeColor = System.Drawing.Color.Green;
            this.labOnlineFlag2.Location = new System.Drawing.Point(735, 419);
            this.labOnlineFlag2.Name = "labOnlineFlag2";
            this.labOnlineFlag2.Size = new System.Drawing.Size(46, 21);
            this.labOnlineFlag2.TabIndex = 70;
            this.labOnlineFlag2.Text = "在线";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("幼圆", 12F);
            this.label29.Location = new System.Drawing.Point(1010, 423);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 16);
            this.label29.TabIndex = 69;
            this.label29.Text = "故障状态：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("幼圆", 12F);
            this.label30.Location = new System.Drawing.Point(804, 423);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 16);
            this.label30.TabIndex = 68;
            this.label30.Text = "输出电流：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("幼圆", 12F);
            this.label31.Location = new System.Drawing.Point(613, 423);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 16);
            this.label31.TabIndex = 67;
            this.label31.Text = "在线状态：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("幼圆", 12F);
            this.label32.Location = new System.Drawing.Point(536, 423);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 16);
            this.label32.TabIndex = 66;
            this.label32.Text = "模块2";
            // 
            // labFaultMod3
            // 
            this.labFaultMod3.AutoSize = true;
            this.labFaultMod3.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labFaultMod3.ForeColor = System.Drawing.Color.Green;
            this.labFaultMod3.Location = new System.Drawing.Point(1132, 460);
            this.labFaultMod3.Name = "labFaultMod3";
            this.labFaultMod3.Size = new System.Drawing.Size(28, 21);
            this.labFaultMod3.TabIndex = 79;
            this.labFaultMod3.Text = "无";
            // 
            // labMod3Cur
            // 
            this.labMod3Cur.AutoSize = true;
            this.labMod3Cur.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labMod3Cur.ForeColor = System.Drawing.Color.Green;
            this.labMod3Cur.Location = new System.Drawing.Point(926, 460);
            this.labMod3Cur.Name = "labMod3Cur";
            this.labMod3Cur.Size = new System.Drawing.Size(60, 21);
            this.labMod3Cur.TabIndex = 78;
            this.labMod3Cur.Text = "50.0A";
            // 
            // labOnlineFlag3
            // 
            this.labOnlineFlag3.AutoSize = true;
            this.labOnlineFlag3.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labOnlineFlag3.ForeColor = System.Drawing.Color.Green;
            this.labOnlineFlag3.Location = new System.Drawing.Point(735, 460);
            this.labOnlineFlag3.Name = "labOnlineFlag3";
            this.labOnlineFlag3.Size = new System.Drawing.Size(46, 21);
            this.labOnlineFlag3.TabIndex = 77;
            this.labOnlineFlag3.Text = "在线";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("幼圆", 12F);
            this.label36.Location = new System.Drawing.Point(1010, 464);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(88, 16);
            this.label36.TabIndex = 76;
            this.label36.Text = "故障状态：";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("幼圆", 12F);
            this.label37.Location = new System.Drawing.Point(804, 464);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(88, 16);
            this.label37.TabIndex = 75;
            this.label37.Text = "输出电流：";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("幼圆", 12F);
            this.label38.Location = new System.Drawing.Point(613, 464);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(88, 16);
            this.label38.TabIndex = 74;
            this.label38.Text = "在线状态：";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("幼圆", 12F);
            this.label39.Location = new System.Drawing.Point(536, 464);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(48, 16);
            this.label39.TabIndex = 73;
            this.label39.Text = "模块3";
            // 
            // labFaultMod4
            // 
            this.labFaultMod4.AutoSize = true;
            this.labFaultMod4.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labFaultMod4.ForeColor = System.Drawing.Color.Green;
            this.labFaultMod4.Location = new System.Drawing.Point(1132, 501);
            this.labFaultMod4.Name = "labFaultMod4";
            this.labFaultMod4.Size = new System.Drawing.Size(28, 21);
            this.labFaultMod4.TabIndex = 86;
            this.labFaultMod4.Text = "无";
            // 
            // labMod4Cur
            // 
            this.labMod4Cur.AutoSize = true;
            this.labMod4Cur.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labMod4Cur.ForeColor = System.Drawing.Color.Green;
            this.labMod4Cur.Location = new System.Drawing.Point(926, 501);
            this.labMod4Cur.Name = "labMod4Cur";
            this.labMod4Cur.Size = new System.Drawing.Size(60, 21);
            this.labMod4Cur.TabIndex = 85;
            this.labMod4Cur.Text = "50.0A";
            // 
            // labOnlineFlag4
            // 
            this.labOnlineFlag4.AutoSize = true;
            this.labOnlineFlag4.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.labOnlineFlag4.ForeColor = System.Drawing.Color.Green;
            this.labOnlineFlag4.Location = new System.Drawing.Point(735, 501);
            this.labOnlineFlag4.Name = "labOnlineFlag4";
            this.labOnlineFlag4.Size = new System.Drawing.Size(46, 21);
            this.labOnlineFlag4.TabIndex = 84;
            this.labOnlineFlag4.Text = "在线";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("幼圆", 12F);
            this.label43.Location = new System.Drawing.Point(1010, 505);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(88, 16);
            this.label43.TabIndex = 83;
            this.label43.Text = "故障状态：";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("幼圆", 12F);
            this.label44.Location = new System.Drawing.Point(804, 505);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(88, 16);
            this.label44.TabIndex = 82;
            this.label44.Text = "输出电流：";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("幼圆", 12F);
            this.label45.Location = new System.Drawing.Point(613, 505);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(88, 16);
            this.label45.TabIndex = 81;
            this.label45.Text = "在线状态：";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("幼圆", 12F);
            this.label46.Location = new System.Drawing.Point(536, 505);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 16);
            this.label46.TabIndex = 80;
            this.label46.Text = "模块4";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("幼圆", 16F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.label17.Location = new System.Drawing.Point(99, 205);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(109, 22);
            this.label17.TabIndex = 87;
            this.label17.Text = "电压模式:";
            this.label17.Click += new System.EventHandler(this.label17_Click_1);
            // 
            // cmbVolType
            // 
            this.cmbVolType.Font = new System.Drawing.Font("幼圆", 16F);
            this.cmbVolType.FormattingEnabled = true;
            this.cmbVolType.Items.AddRange(new object[] {
            "低压模式",
            "高压模式"});
            this.cmbVolType.Location = new System.Drawing.Point(268, 202);
            this.cmbVolType.Name = "cmbVolType";
            this.cmbVolType.Size = new System.Drawing.Size(195, 29);
            this.cmbVolType.TabIndex = 88;
            this.cmbVolType.SelectedIndexChanged += new System.EventHandler(this.cmbVolType_SelectedIndexChanged);
            // 
            // setting_uc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cmbVolType);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.labFaultMod4);
            this.Controls.Add(this.labMod4Cur);
            this.Controls.Add(this.labOnlineFlag4);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.labFaultMod3);
            this.Controls.Add(this.labMod3Cur);
            this.Controls.Add(this.labOnlineFlag3);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.labFaultMod2);
            this.Controls.Add(this.labMod2Cur);
            this.Controls.Add(this.labOnlineFlag2);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.labFaultMod1);
            this.Controls.Add(this.labMod1Cur);
            this.Controls.Add(this.labOnlineFlag1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbCtrMode);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.labInCur);
            this.Controls.Add(this.labInHz);
            this.Controls.Add(this.labInVW);
            this.Controls.Add(this.labInWU);
            this.Controls.Add(this.labInUV);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtSetCur5);
            this.Controls.Add(this.txtSetVol5);
            this.Controls.Add(this.txtSetCur4);
            this.Controls.Add(this.txtSetVol4);
            this.Controls.Add(this.txtSetCur3);
            this.Controls.Add(this.txtSetVol3);
            this.Controls.Add(this.txtSetCur2);
            this.Controls.Add(this.txtSetVol2);
            this.Controls.Add(this.txtSetCur1);
            this.Controls.Add(this.txtSetVol1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbComList);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "setting_uc";
            this.Size = new System.Drawing.Size(1203, 608);
            this.Load += new System.EventHandler(this.setting_uc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbComList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSetVol1;
        private System.Windows.Forms.TextBox txtSetCur1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSetCur2;
        private System.Windows.Forms.TextBox txtSetVol2;
        private System.Windows.Forms.TextBox txtSetCur3;
        private System.Windows.Forms.TextBox txtSetVol3;
        private System.Windows.Forms.TextBox txtSetCur4;
        private System.Windows.Forms.TextBox txtSetVol4;
        private System.Windows.Forms.TextBox txtSetCur5;
        private System.Windows.Forms.TextBox txtSetVol5;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labInCur;
        private System.Windows.Forms.Label labInHz;
        private System.Windows.Forms.Label labInVW;
        private System.Windows.Forms.Label labInWU;
        private System.Windows.Forms.Label labInUV;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbCtrMode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labOnlineFlag1;
        private System.Windows.Forms.Label labMod1Cur;
        private System.Windows.Forms.Label labFaultMod1;
        private System.Windows.Forms.Label labFaultMod2;
        private System.Windows.Forms.Label labMod2Cur;
        private System.Windows.Forms.Label labOnlineFlag2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labFaultMod3;
        private System.Windows.Forms.Label labMod3Cur;
        private System.Windows.Forms.Label labOnlineFlag3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label labFaultMod4;
        private System.Windows.Forms.Label labMod4Cur;
        private System.Windows.Forms.Label labOnlineFlag4;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbVolType;
    }
}
