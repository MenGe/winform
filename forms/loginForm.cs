﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dc_power_monitor.forms;

namespace dc_power_monitor
{
    
    public partial class formLogin : Form
    {

        userHelper helper = new userHelper("");
        List<user> ListUser = new List<user>();

        public user nowUser = new user();
        recordHelper rec;
        public formLogin()
        {
            InitializeComponent();

            helper.checkSuperUser("user.pt", ListUser);

            rec = new recordHelper("log_opreat_fault.db");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var res = helper.checkUserLogin("user.pt", tbLoginUser.Text, tbLoginPasswd.Text);
            if(res == null)
            {
                labLoginMes.Text = "账号或密码错误，请重新输入！";
                labLoginMes.ForeColor = System.Drawing.Color.Red;
                return;
            }

            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//获取Configuration对象
            config.AppSettings.Settings["user"].Value = res.username;//key的值取textbox.text
            config.Save(ConfigurationSaveMode.Modified);//保存
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");



            nowUser = res;
            rec.addOpreatRecord(res.username, "登录");
            homeForm anotherForm = new homeForm(nowUser);                        //定义新窗体
            this.Hide();                                      //隐藏旧窗体
            anotherForm.ShowDialog();                         //展示新窗体
            this.Close();
            Application.ExitThread();


            //this.DialogResult = DialogResult.OK;
            //this.Dispose();
            //this.Close();
        }

        private void tbLoginPasswd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(null,null);
            }

        }
    }
}
