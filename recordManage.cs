﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Data;

namespace UI_study
{
    


    public class recordHelper
    {
        private sqlHelper record;

        public recordHelper()
        {

            try
            {
                // passWord = new SqLiteHelper("data source=password.db");
                record = new sqlHelper("Log1.db");
                //创建名为table1的数据表
                record.CreateTable("opreat", new string[] { "index", "user", "date", "operat" }, new string[] { "INTEGER", "TEXT", "TEXT", "TEXT" });
                record.CreateTable("fault", new string[] { "index", "date", "fault" }, new string[] { "INTEGER", "TEXT", "TEXT" });
                //插入两条数据
                //passWord.InsertValues("password", new string[] { "小三", "123456" });

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                record.CloseConnection();
            }
           
        }

        public DataTable getFaultRecord()
        {
            try
            {
                SQLiteDataAdapter mAdapter = new SQLiteDataAdapter("SELECT * FROM fault", record.dbConnection);
                DataTable dt = new DataTable();
                mAdapter.Fill(dt);
           
                return dt;
            }
            catch
            {
                return null;
            }
            
        }

        public DataTable getOpreatRecord()
        {

            try
            {
                SQLiteDataAdapter mAdapter = new SQLiteDataAdapter("SELECT * FROM opreat", record.dbConnection);
                DataTable dt = new DataTable();
                mAdapter.Fill(dt);

                return dt;
            }
            catch
            {
                return null;
            }
        }

        public void addOpreatRecord(string user, string opreat)
        {
            SQLiteDataReader tab = record.ReadFullTable("opreat");

            int index = 0;

            if(tab != null)
            {
                while (tab.Read())
                {
                    index = tab.GetInt32(0);
                }
            }
            

            record.InsertValues("opreat", new string[] { index.ToString(), user, DateTime.Now.ToString("u"), opreat });
        }

        public void addFaultRecord(string fault)
        {
            SQLiteDataReader tab = record.ReadFullTable("fault");

            int index = 0;

            while (tab.Read())
            {
                index = tab.GetInt32(0);
            }

            record.InsertValues("fault", new string[] { index.ToString(), DateTime.Now.ToString("u"), fault });
        }
    }
}
