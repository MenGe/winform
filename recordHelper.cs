﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dc_power_monitor
{
    public class recordHelper
    {
        SQLiteConnection mConn;
         
        

        public recordHelper(string path)
        {
            string mDbPath = Application.StartupPath + "\\" + path;
            mConn = new SQLiteConnection("Data Source=" + mDbPath);
            mConn.Open();

            _sqlExecute("CREATE TABLE IF NOT EXISTS fault(日期 TEXT, 故障 TEXT);");
            _sqlExecute("CREATE TABLE IF NOT EXISTS opreat(日期 TEXT, 用户 TEXT, 操作 TEXT);");
           
        }

        private void _sqlExecute(string sql)
        {
            using (SQLiteCommand mCmd = new SQLiteCommand
                (sql,
                 mConn))
            {
                mCmd.ExecuteNonQuery();
            }
        }

        public void delTable(string table)
        {
            string sql = "DELETE FROM " + table + ";";
            _sqlExecute(sql);
        }


        public void addOpreatRecord(string user, string opreat)
        {
            string sql = "INSERT INTO opreat VALUES (" + "'"+DateTime.Now.ToString("") + "','" + user + "','" + opreat + "');";
            _sqlExecute(sql);
        }

        public void addFaultRecord(string fault)
        {
            string sql = "INSERT INTO fault VALUES ('" + DateTime.Now.ToString("") + "','" + fault + "');";
            _sqlExecute(sql);
        }

        public DataTable getOpreatRecord()
        {
            SQLiteDataAdapter mAdapter = new SQLiteDataAdapter("SELECT * FROM opreat", mConn);
            DataTable dt = new DataTable();
            mAdapter.Fill(dt);
            return  dt;
        }

        public DataTable getFaultRecord()
        {
            SQLiteDataAdapter mAdapter = new SQLiteDataAdapter("SELECT * FROM fault", mConn);
            DataTable dt = new DataTable();
            mAdapter.Fill(dt);
            return dt;
        }
    }
}
