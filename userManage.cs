﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace dc_power_monitor
{
    [Serializable]
    public class user
    {
        public int index { get; set; }

        public string username { get; set; }

        public string passwd { get; set; }

        public authority level { get; set; }

    }

    public enum authority
    {
        管理员,
        工程师,
    }

    public class userHelper
    {
        private string filePath = string.Empty;

        public userHelper(string path)
        {
            filePath = path;
        }

        /// <summary>
        /// 序列化到文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="listUser"></param>
        /// <returns></returns>
        public bool serializedUser(string path, List<user> listUser)
        {
            if(listUser == null)
            {
                return false;
            }

            BinaryFormatter format = new BinaryFormatter();
            using(FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                format.Serialize(fs, listUser);
                return true;
            }
        }
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<user> DeSerializedUser(string path)
        {
            BinaryFormatter format = new BinaryFormatter();

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    object res = format.Deserialize(fs);
                    return res as List<user>;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// 创建超级用户
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="listuser"></param>
        public void checkSuperUser(string path ,List<user> listuser)
        {
            if (!File.Exists(path))
            {
                user use = new user()
                { index = 0, username = "admin", passwd = "123456", level = authority.管理员 };
                listuser.Add(use);
                serializedUser(path, listuser);
            }
        }
        /// <summary>
        /// 检查重复
        /// </summary>
        /// <param name="list"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool checkContainUser(List<user> list,string userName)
        {
            var user = from item in list
                       where item.username == userName
                       select item;
            if(user.Count() > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="path"></param>
        /// <param name="list"></param>
        /// <param name="User"></param>
        /// <returns></returns>
        public bool addUser(string path,List<user> list,user User)
        {
            if(User == null)
            {
                return false;
            }
            if(checkContainUser(list, User.username))
            {
                return false;
            }
            list.Add(User);
            serializedUser(path, list);
            return true;
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="path"></param>
        /// <param name="list"></param>
        /// <param name="User"></param>
        /// <returns></returns>
        public bool delUser(string path, List<user> list, string User)
        {
            if(list == null)
            {
                return false;
            }

            int index = 0;
            foreach(var item in list)
            {
                if(item.username == User)
                {
                    break;
                }
                index++;
            }
            if (index == 0 | index >= list.Count) return false;
            list.RemoveAt(index);
            serializedUser(path, list);
            return true;
            
        }


        public user checkUserLogin(string path, string username , string passwd)
        {
            if(!File.Exists(path))
            {
                return null;
            }
            List<user> list = DeSerializedUser(path);
            var res = from item in list
                      where item.username == username && item.passwd == passwd
                      select item;

            if(res.Count() == 0)
            {
                return null;
            }

            user u = new user();
            foreach (var item in res)
            {
                u.username = item.username;
                u.passwd = item.passwd;
                u.level = item.level;
            }

            return u;
        }
    }
}
