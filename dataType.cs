﻿

namespace dc_power_monitor
{
	public class RootObject
	{
		public string cmd { get; set; }
		public bool sta { get; set; }
		public string vol { get; set; }
		public string cur { get; set; }
		public string pow { get; set; }
		public string vSet { get; set; }
		public string cSet { get; set; }
		public string eCode { get; set; }
		public bool ctrmod { get; set; }
		public bool volmod { get; set; }
		public bool dissta { get; set; }
		public bool[] devsta { get; set; }
		public ushort[] errcode { get; set; }
	}

	public class inputParam
	{
		public string inCur { get; set; }
		public string inHz { get; set; }
		public string inUV { get; set; }
		public string inVW { get; set; }
		public string inWU { get; set; }
	}

	public class modParam
	{
		public bool[] mod_online { get; set; }
		public ushort[] mod_out_i { get; set; }
		public bool[] mod_fault { get; set; }
	}


	public class outputSet
	{
		public string cmd { get; set; }	
		public int vol_set { get; set; }
		public int cur_set { get; set; }
	}

	public class powerOnOff
	{
		public string cmd { get; set; }
		public string sw { get; set; }
	}

	public class ctrModSwitch
	{
		public string cmd { get; set; }
		public string mod { get; set; }
	}
}
